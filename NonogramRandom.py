#!/usr/bin/env python3

from random import random

class NonogramRandom():

    def __init__(self, x=15, y=15):
        self.sizeX = x
        self.sizeY = y

        self.n = [
            [
                0 if random() < 0.5 else 1 for i in range(self.sizeY)
            ] for i in range(self.sizeX)
        ]

    def getCellCountForRow(self, row):
        counts = []
        i = 0

        for cell in row:
            if cell == 1:
                i = i + 1
            elif cell == 0 and i != 0:
                counts.append(i)
                i = 0

        if i != 0:
            counts.append(i)

        return " ".join([str(c) for c in counts])


    def getColumnHeader(self):
        counts = []

        for j in range(self.sizeX):
            i = 0
            foo = []
            for row in self.n:
                cell = row[j]
                if cell == 1:
                    i = i + 1
                elif cell == 0 and i != 0:
                    foo.append(i)
                    i = 0

            if i != 0:
                foo.append(i)

            counts.append(foo)

        maxLen = len(max(counts, key=lambda x: len(x)))
        s = ""
        for i in range(maxLen):
            for colHead in counts:
                try:
                    s = s + str(colHead[i])
                except IndexError as e:
                    s = s + " "
                s = s + " "
            s = s + "\n"

        return s.lstrip()

    def __str__(self):
        colHeads = self.getColumnHeader().split("\n")
        colHeads = [c for c in colHeads if len(c)]
        colHeads.reverse()
        colHead = "\n".join([c.rjust(15 + self.sizeX * 2 + 1) for c in colHeads]) + "\n"

        s = colHead
        for row in self.n:
            rowHead = str(self.getCellCountForRow(row))
            rowHead = rowHead.rjust(15) + " "
            s = s + rowHead
            for cell in row:
                if cell == 0:
                    s = s + " "
                elif cell == 1:
                    s = s + "o"
                s = s + "."
            s = s + "\n"

        s = s.rstrip()

        return s